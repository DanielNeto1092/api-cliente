FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/api-cliente-*.jar app.jar
ENTRYPOINT ["java", "-Xmx300m -Xss512k -XX:CICompilerCount=2 -Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]